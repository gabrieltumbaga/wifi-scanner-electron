var currentlyRunning = false;
var interval;
var ipc = require('electron').ipcRenderer;
var scanButton = document.getElementById('scan-button');



scanButton.addEventListener('click', function(){
    if (scanButton.value == "On") {
        scanButton.value = "Off"
        $('#myModal').modal('show');
        currentlyRunning = true;

        interval = setInterval(function() {
            document.getElementById('status').innerHTML = " ";
            ipc.once('actionReply', function(event, response){
                processResponse(response);
            });
           ipc.send('invokeAction', 'getWifiList');
       }, 2000);
    } else {
        scanButton.value = "On";
        document.getElementById('status').innerHTML = "stopped.";
        currentlyRunning = false;
        clearInterval(interval);
    }





});

function scrollTo(myID) {
    var topBarHeight = 110;
    $("html, body").animate({ scrollTop: $('#' + myID).offset().top - topBarHeight }, 500);
}

function processResponse(response) {
    document.getElementById('response').innerHTML = response;
    document.getElementById('status').innerHTML = "running...";

    $('#mainGraph').html('');
    $('#mainGraph5').html('');
    $('#mainGraphdfs').html('');

    jQuery.each(JSON.parse(response), function(index, value) {
        mySSID = (value.ssid == "") ? "(Hidden)" : value.ssid;
        myMacAddress = value.mac;
        mySignalStrength = value.rssi;
        mySignalStrengthPerc = (2 * (mySignalStrength + 100) * 2);
        myCalculatedzIndex = 100 - (2 * (mySignalStrength + 100));
        myCleanMac = myMacAddress.replace(/:/g,'');
        myChannel = value.channel;

        var accepted_24 = [1,2,3,4,5,6,7,8,9,10,11,12,13];
        var accepted_50 = [36,38,40,42,44,46,48,149,151,153,155,157,159,161,165];

        if (contains.call(accepted_50, myChannel)) {
            //5ghz
            $('#mainGraph5').append('<div id="' + myCleanMac + '" class="wifiNetwork channel-' + myChannel + '">' + mySSID + '</div>');
        } else if (contains.call(accepted_24, myChannel)) {
            //2.4ghz
            $('#mainGraph').append('<div id="' + myCleanMac + '" class="wifiNetwork channel-' + myChannel + '">' + mySSID + '</div>');
        } else {
            //dfs
            $('#mainGraphdfs').append('<div id="' + myCleanMac + '" class="wifiNetwork channel-dfs">' + mySSID + '</div>');
        }

        $('#' + myCleanMac).css('height', mySignalStrengthPerc + 'px');
        $('#' + myCleanMac).css('z-index', myCalculatedzIndex);
    });
}






var contains = function(needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle;
    var indexOf;

    if(!findNaN && typeof Array.prototype.indexOf === 'function') {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function(needle) {
            var i = -1, index = -1;

            for(i = 0; i < this.length; i++) {
                var item = this[i];

                if((findNaN && item !== item) || item === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle) > -1;
};
