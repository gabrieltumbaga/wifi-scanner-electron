<img src="/img/crew.jpg" class="img-responsive" />
<h1 class="text-center">About Us</h1>
<p>
    Macrobuild Corporation, led by Gil Legarte and Lito Alviz, is a group of fun loving, family oriented individuals that take the quality of their work very seriously.  When you hire this San Francisco / Peninsula area based team, not only will you find that their work is outstanding, but also their ideals.  These talented and loyal professionals will provide you with results that can be considered a work of art.   Every step along the way, you will be kept notified of time and cost estimations, so you'll never be left in the dark.  You'll find that working with Macrobuild Corporation will be an enjoyable, pleasant experience. 
</p>