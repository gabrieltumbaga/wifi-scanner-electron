<?php
    $myImgCollection = array();
    $myCounter = 0;
    // $myImgCollection['uri'] = array();
    // $myImgCollection['desc'] = array();

    

    //set 4
    for ($k = 0 ; $k < 7; $k++) { 
        $picNumber = $k + 1;
        $myImgCollection[] = array(
            "imgName" => "longside_1280/export4-" . $picNumber . ".jpg",
            "thumbName" => "tn/tn_export4-" . $picNumber . ".jpg",
            "description" => ""
        ); 
    }

    //set 3
    for ($k = 0 ; $k < 11; $k++) { 
        $picNumber = $k + 1;
        $myImgCollection[] = array(
            "imgName" => "longside_1280/export3-" . $picNumber . ".jpg",
            "thumbName" => "tn/tn_export3-" . $picNumber . ".jpg",
            "description" => ""
        ); 
    }

    //set 2b
    for ($k = 0 ; $k < 4; $k++) { 
        $picNumber = $k + 1;
        $myImgCollection[] = array(
            "imgName" => "longside_1280/export2b-" . $picNumber . ".jpg",
            "thumbName" => "tn/tn_export2b-" . $picNumber . ".jpg",
            "description" => ""
        ); 
    }

    //set 1
    for ($k = 0 ; $k < 24; $k++) { 
        $picNumber = $k + 1;
        $myImgCollection[] = array(
            "imgName" => "longside_1280/export1-" . $picNumber . ".jpg",
            "thumbName" => "tn/tn_export1-" . $picNumber . ".jpg",
            "description" => ""
        ); 
    }
    
    // $myImgCollection[] = array(
    //     "imgName" => "1280x720",
    //     "thumbName" => "250x250",
    //     "description" => "This is my Description"
    // );
    // $myImgCollection[] = array(
    //     "imgName" => "720x1280",
    //     "thumbName" => "250x250",
    //     "description" => "This is my Description"
    // );
    // $myImgCollection[] = array(
    //     "imgName" => "1280x720",
    //     "thumbName" => "250x250",
    //     "description" => "This is my Description"
    // );
?>

<div class="top-40">
    <h1 class="text-center">Image Gallery</h1>
    

    <div class="zoom-gallery">
        <div class="row">

            <?php
                //$urlPrefix = "http://placehold.it/";
                $urlPrefix = "/img/gallery/";
            ?>

            <?php foreach ($myImgCollection as $imgItem): ?>
                <div class="col-sm-3 col-xs-6">

                    <?php
                        $myImgURL = $urlPrefix . $imgItem['imgName'];
                        $myImgURLthumb = $urlPrefix . $imgItem['thumbName'];
                        $myDesc = $imgItem['description'];
                    ?>
                    <a href="<?php print $myImgURL; ?>" title="<?php print $myDesc; ?>">
                        <img class="thumb" src="<?php print $myImgURLthumb; ?>" />
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>




