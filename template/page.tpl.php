<div id="top-black">
    <div class="constrain">
        <div class="inner clearfix">
            <div class="pull-left">
                <div class="logo-holder stroke">Macrobuild Corporation</div>
            </div>
            <div class="pull-right">
                <div class="other-text text-right">
                    <!--<div class="top-row">Construction &bull; Engineering &bull; Design</div>-->
                    <div class="top-row">General Contractor</div>
                    <div class="bot-row">Lic. #1024387</div>
                </div>
            </div>
        </div>
    </div>
</div>

<nav id="gnav" class="navbar constrain less navBG">
    <div class="container constrain ">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <?php
                foreach($menuArray['menu1'] as $menuKey => $menuData) {
                    print "<li class='{$menuData['class']}'><a href='{$menuData['path']}'>{$menuKey}</a></li>";
                }
            ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php
                foreach($menuArray['menu2'] as $menuKey => $menuData) {
                    print "<li class='{$menuData['class']}'><a href='{$menuData['path']}'>{$menuKey}</a></li>";
                }
            ?>
        </ul>
    </div><!--/.nav-collapse -->
    </div>
    <div class="circle logoCircle"></div>
</nav>


<div class="container constrain">
    <?php
        //include the node
        $includeFile = $docRoot . "/template/" . $siteSettings['template_files']['node'];
        include_once($includeFile);
    ?>
</div><!-- /.container -->


<div class="footer-area">
    <div class="container constrain">
        <div class="row">
            <div class="col-xs-6 text-left mobile-text-center">
                &copy; 2017 Macrobuild Corporation.  All Rights Reserved.
            </div>
            <div class="col-xs-6 text-right mobile-text-center">
                License #1024387
            </div>
        </div>
    </div>
</div>



