<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Macrobuild Corporation</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.min.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->

    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
</head>

<body>
    <?php
        $includeFile = $docRoot . "/template/" . $siteSettings['template_files']['page'];
        include_once($includeFile);
    ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/jquery.1.12.4.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>

    <?php if ($routes[1] == 'gallery'): ?>
         
        <script src="/js/jquery.magnific-popup.min.js"></script>

        <script>
        jQuery(document).ready(function() {
            jQuery('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true,
                    titleSrc: function(item) {
                        return item.el.attr('title') + '<a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
                    }
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                        return element.find('img');
                    }
                },
                callbacks: {
                    imageLoadComplete: function() {
                        console.log(this);
                        myWidth = this.currItem.img.context.naturalWidth;
                        myHeight = this.currItem.img.context.naturalHeight;

                        if (myHeight > myWidth) {
                            jQuery(".mfp-figure img").addClass('portrait');
                        } else {
                            jQuery(".mfp-figure img").removeClass('portrait');
                        }


                        //console.log("img Size: " + myWidth + "x" + myHeight);


                    }
                }

            });

            //check if portrait, and give class

            
        });


        </script>
    <?php endif; ?>
</body>
</html>
