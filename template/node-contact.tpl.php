<img src="/img/partners.jpg" class="img-responsive" />
<h1 class="text-center">Contact Us</h1>
<br/><br/>
<p class="lead">
    For Inquiries please call customer service: <a href="tel://14159885838">+1(415)988-5838</a>.<br/><br/>
</p>
    <h3>Hours of Operation</h3>
    <hr />
    Monday - Friday:<br/>
    8:00am - 5:00pm PT<br/>
    <br/>
    Saturday:<br/>
    8:00am - 3:00pm PT<br/>
<br/><br/>
<div class="row contact-holder">
    <div class="col-sm-12 push-40"> 
        <h3>Gil Legarte</h3>
        <hr />
        <div class="contact-row">
            <div class="label">
                <span class="glyphicon glyphicon-envelope"></span> Email
            </div>
            <div class="data">
                <a href="mailto:gil@macrobuildcorp.com">gil@macrobuildcorp.com</a>
            </div>
        </div>
        <div class="contact-row">
            <div class="label">
                <span class="glyphicon glyphicon-phone"></span> Work
            </div>
            <div class="data">
                <a href="tel://14159885838">+1 (415) 988-5838</a>
            </div>
        </div>
        <div class="contact-row">
            <div class="label">
                <span class="glyphicon glyphicon-phone"></span> Mobile
            </div>
            <div class="data">
                <a href="tel://5103679542">+1 (510) 367-9542</a>
            </div>
        </div>
    </div>
    <div class="col-sm-12 push-40"> 
        <h3>Lito Alviz</h3>
        <hr />
        <div class="contact-row">
            <div class="label">
                <span class="glyphicon glyphicon-envelope"></span> Email
            </div>
            <div class="data">
                <a href="mailto:lito@macrobuildcorp.com">lito@macrobuildcorp.com</a>
            </div>
        </div>
        <div class="contact-row">
            <div class="label">
                <span class="glyphicon glyphicon-phone"></span> Work
            </div>
            <div class="data">
                <a href="tel://14159885793">+1 (415) 988-5793</a>
            </div>
        </div>
        <div class="contact-row">
            <div class="label">
                <span class="glyphicon glyphicon-phone"></span> Mobile
            </div>
            <div class="data">
                <a href="tel://7077044745">+1 (707) 704-4745</a>
            </div>
        </div>
    </div>
</div>