<div class="carousel-holder push-40">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="/img/carousel/interior1x720.jpg" alt="">
                <!-- <div class="carousel-caption">
                    <h2>CAPTION 1</h2>
                    <p>This is the caption for this slide...</p>
                </div> -->
            </div>
            
            <div class="item">
                <img src="/img/carousel/interior2x720.jpg" alt="">
                <!-- <div class="carousel-caption">
                    <h2>CAPTION 2</h2>
                    <p>This is the caption for this slide...</p>
                </div> -->
            </div>

            <div class="item">
                <img src="/img/carousel/interior3x720.jpg" alt="">
                <!-- <div class="carousel-caption">
                    <h2>CAPTION 3</h2>
                    <p>This is the caption for this slide...</p>
                </div> -->
            </div>
            
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>


<div class="text-center push-40">
    <h1>Dream Big</h1>
    <p class="lead">
        Welcome to Macrobuild Corporation. We are not your average general contractor, we're masters of our craft, and strive to achieve your goals when you dream big.
    </p>
    <p class="lead">
        When you choose us, we're more than just your general contractor, we're your friend. Not only will you enjoy your final product; you'll enjoy the journey!
    </p>
</div>

<!-- <div class="row push-40">
    <div class="col-sm-4">
        <h4>Pod Title</h4>
        <p>
            This is the info pod.  Put some info in here.  Info is good.
        </p>
        <div class="clearfix">
            <button class="btn btn-default pull-right">Read &raquo;</button>
        </div>
        
    </div>
    <div class="col-sm-4">
        <h4>Pod Title</h4>
        <p>
            This is the info pod.  Put some info in here.  Info is good.
        </p>
        <div class="clearfix">
            <button class="btn btn-default pull-right">Read &raquo;</button>
        </div>
    </div>
    <div class="col-sm-4">
        <h4>Pod Title</h4>
        <p>
            This is the info pod.  Put some info in here.  Info is good.
        </p>
        <div class="clearfix">
            <button class="btn btn-default pull-right">Read &raquo;</button>
        </div>
    </div>
</div> -->